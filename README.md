# hello-grpc


## Build stub and skeleton from proto file

Change to root directory of project and generate stub and skeloton go file:
```
$ go generate ./...
```

## Start Server

Change to directory hello_server and run the server by calling:
```
$ go run main.go
2024/06/16 16:02:28 server listening at [::]:9111
```

## Start Client

Change to directory hello_client and run the client by calling:
```
$ go run main.go
```
You are allowed to pass a name using the command line argument:

```
$ go run main.go Martin
```

The output should be like this:
```
schmolli@INF-ND-SCHMOLLI:~/go-projects/hello-grpc/hello_client$ go run main.go
2024/06/16 16:02:39 Greeting: Hello world
schmolli@INF-ND-SCHMOLLI:~/go-projects/hello-grpc/hello_client$ go run main.go Martin
2024/06/16 16:02:41 Greeting: Hello Martin
```
